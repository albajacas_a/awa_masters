# Author: Matthias Frey
import json
from collections import OrderedDict
import os
import re
import linecache
import argparse


def parse_sampler_file(fname):
    dvar_pattern = r'.*DVAR.*VARIABLE[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*["](.*)["].*' \
        'LOWERBOUND[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*(.*)[\t]*[ ]*[\t]*,[\t]*[ ]*[\t]*' \
            'UPPERBOUND[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*(.*)[\t]*[ ]*[\t]*[;]'
    
    outdir_pattern  = r'[\t]*[ ]*[\t]*OUTDIR[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*["](.*)["]'
    outfile_pattern  = r'[\t]*[ ]*[\t]*OUTPUT[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*["](.*)["]'
    tmpldir_pattern = r'[\t]*[ ]*[\t]*TEMPLATEDIR[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*["](.*)["]'
    tmpl_pattern    = r'[\t]*[ ]*[\t]*INPUT[\t]*[ ]*[\t]*[=][\t]*[ ]*[\t]*["](.*)["]'
    
    dirname = ''
    tmpldir = ''
    tmpl    = ''
    outfile = ''
    dvars = []
    lower_bounds = []
    upper_bounds = []
    for i, line in enumerate(open(fname)):
        obj = re.match(dvar_pattern, line)
        if obj:
            dvars.append( obj.group(1) )
            lower_bounds.append( obj.group(2) )
            upper_bounds.append( obj.group(3) )
        obj = re.match(outdir_pattern, line)
        if obj:
            dirname = obj.group(1)
        obj = re.match(tmpldir_pattern, line)
        if obj:
            tmpldir = obj.group(1)
        obj = re.match(tmpl_pattern, line)
        if obj:
            tmpl = obj.group(1)
        obj = re.match(outfile_pattern, line)
        if obj:
            outfile = obj.group(1)
    
    print ( "\ndesign variables found:" )
    for i in range(len(dvars)):
        print ( "\t", dvars[i], "\t[", lower_bounds[i], ", ", upper_bounds[i], "]" )
    print ( )
    print ( "directory to put JSON: ", dirname, '\n' )
    tmpl_file = os.path.join(tmpldir, tmpl)
    print ( "template file: ", tmpl_file, '\n' )
    
    return dvars, lower_bounds, upper_bounds, dirname, tmpl_file, outfile


def parse_template(dvars, tmpl):
    # find patterns
    patterns = []
    for dvar in dvars:
        patterns.append( r'([_A-Za-z]+)[\t]*[ ]*[\t]*[=]*[\t]*[ ]*[\t]*["]*_' + dvar + '_["]*' )
    
    pre_tags = {}
    post_tags = {}
    lines = []
    for i, line in enumerate(open(tmpl_file)):
        for j, dvar in enumerate(dvars):
            if f'_{dvar}_' in line:
                the_split = line.split(f'_{dvar}_')
                pre_tags[dvars[j]] = the_split[0]
                if len(the_split) > 1:
                    post_tags[dvars[j]] = the_split[1]
                lines.append(i+1)
    return pre_tags, post_tags, lines


parser = argparse.ArgumentParser(description='Write Sampler output JSON file. Has to be run in each controllable directory.')

parser.add_argument('--input_file', type=str, nargs=1,
                    help='sampler input file')

args = parser.parse_args()

input_file   = args.input_file[0]


print ( "sampler input file:    ", input_file )

# list of design variable names,
# their lower bounds,
# their upper bounds,
# the output directory,
# template file,
# the JSON file base name to be written
dvars, lower_bounds, upper_bounds, dirname, tmpl_file, outfile = parse_sampler_file(input_file)

print(dvars)
print(dirname)

# what comes before the DVAR placeholder,
# what comes after the DVAR placeholder,
# the line on which the DVAR placeholder is
pre_tags, post_tags, lines = parse_template(dvars, tmpl_file)

print(pre_tags)
print(lines)
print(post_tags)


data = {}
data['name'] = 'sampler'
data['OPAL version'] = '2.1.0'
data['git revision'] = 'unknown'

data['dvar-bounds'] = {}
for i, dvar in enumerate(dvars):
    data['dvar-bounds'][dvar] = str([float(lower_bounds[i]), float(upper_bounds[i])])

# collect samples
data['samples'] = {}

# OPAL input filename
infile = os.path.basename(tmpl_file)
infile = os.path.splitext(infile)[0] + '.in'

for d in os.listdir(dirname):
    dd = os.path.join(dirname, d)
    if os.path.isdir(dd):
        fname = os.path.join(dd, infile)
        data['samples'][d] = {}
        data['samples'][d]['dvar'] = {}

        for l in lines:
            line = linecache.getline(fname, l)
            for key, val in pre_tags.items():
                if val in line:
                    value = line.replace(val, '')
                    value = value.replace(post_tags[key], '')
                    data['samples'][d]['dvar'][key] = value

save = os.path.join(dirname, outfile + '_samples_0.json')
print ( 'save file: ', save )
with open(save, 'w') as outfile:
    json.dump(data, outfile, sort_keys=False, indent=4)
