import matplotlib.pyplot as plt
import numpy as np

fn = 'beam_path_comoving.png'
def arrow(n, step, ax, z, x):
    prop = dict(arrowstyle="-|>,head_width=0.4,head_length=0.8",
                shrinkA=0,shrinkB=0)
    ax.annotate("", xy=(z[n+step],x[n+step]), xytext=(z[n],x[n]), arrowprops=prop)
#    ax.arrow(z[n], x[n], z[n+step] - z[n], x[n+step] - x[n], lw=2, length_includes_head = True, head_width=.95, head_length = 1.5)
    
c = 1
fig, ax = plt.subplots(1,2, sharey = True)
# Lab frame
N = int(1e3)
z = np.linspace(0, 30, N)
x = np.sin(z)
ax[0].plot(z, x)
ax[0].set_xlabel('z_lab', fontsize = 14)
ax[0].set_ylabel('x', fontsize = 14)
ax[0].text(.3, 1.05, 'Laboratory Frame', transform = ax[0].transAxes)
arrow(100, 10, ax[0], z, x)
arrow(630, 10, ax[0], z, x)
# Comoving frame
t = np.zeros(len(x))
for i,_ in enumerate(z):
    if i == 0:
        continue
    t[i] = t[i-1] + np.sqrt((x[i] - x[i-1])**2 + (z[i] - z[i-1])**2)
beta = 1 / c * z[-1] / t[-1]
beta *= .9991
gamma = 1 / np.sqrt(1 - beta**2)
zp = gamma * (z - beta * c * t)
# xp = np.sin(zp)
xp = x
ax[1].plot(zp,xp)
ax[1].set_xlabel('z_comoving', fontsize = 14)
ax[1].text(.3, 1.05, 'Comoving Frame', transform = ax[1].transAxes)
arrow(110, 10, ax[1], zp, xp)
arrow(630, 10, ax[1], zp, xp)
plt.savefig(fn)

plt.show()
