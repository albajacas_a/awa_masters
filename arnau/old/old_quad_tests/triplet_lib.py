#!/usr/bin/env python

import json
import sys
import numpy as np
import sympy as sym


def getQuad(g):
    '''
    g : (float) Quadrupole strength g = L * q * K / p0, with L quad length, q electron charge, K quad gradient, p0 = norm(px,py,pz).
    '''
    return np.array([[1, 0, 0, 0],
                     [g, 1, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, -g, 1]])
def getDrift(l):
    '''
    l : (positive float) Quadrupole length
    '''
    assert l > 0, 'The length of a drift must be positive'
    return np.array([[1, l, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, l],
                     [0, 0, 0, 1]])
def getInitDrift(init):
    '''
    Returns a virtual additional drift necessary so that the solver can assume that the bunch was initially at (0, px/p0).
    init : (1x2 np.array) Initial phase-space coordinates (x, px/p0)
    Note that it is assumed that (x, px/p0) = (y, py/p0).
    '''
    assert len(init) == 2, 'init = (x, px/p0) must be a 1x2 np.array).'
    assert abs(init[1]) <= 1, 'the momentum must be between -1 and 1, since it is px/p0.'
    assert init[1] > 0, 'At the moment this code only works for positive transerse momenta (i.e. beam with a positive xpx correlation)'

    alpha = np.arcsin(init[1])
    l = init[0] / np.tan(alpha)

    print('Adding initial virtual drift of length ' + str(l) + ' m')
    return getDrift(l)

def getProblem(Ds, Gs, init):
    '''
    Ds : (list of floats) Drift lengths. Must be len(Ds) = ln(Gs) or len(Ds) = ln(Gs) + 1 if you want to have a drift in front of the first quad.
    Gs : (list of symbols and floats) Quad focussing strengths.
    init : (1x2 np.array) Initial phase-space coordinates (x, px/p0)
    Note that it is assumed that (x, px/p0) = (y, py/p0).
    '''
    assert len(init) == 2, 'init = (x, px/p0) must be a 1x2 np.array).'
    assert abs(init[1]) <= 1, 'the momentum must be between -1 and 1, since it is px/p0.'

    print('Creating matrix...')
    # Create matrix
    Ms = []
    Ms.insert( 0, getInitDrift(init) )
    init = np.array([0, init[1], 0, init[1]])
    
    if len(Ds) == len(Gs) + 1:
        Ms.insert(0, getDrift(Ds[0]))
        Ds = Ds[1:]
    assert len(Ds) == len(Gs), 'Must be len(Ds) = ln(Gs) or len(Ds) = ln(Gs) + 1 if you want to have a drift in front of the first quad.'

    for i,_ in enumerate(Ds):
        Ms.insert(0, getQuad(Gs[i]))
        Ms.insert(0, getDrift(Ds[i]))

    M = np.linalg.multi_dot(Ms)

    # Assert symplecticity
    Mtest = sym.Matrix(M).subs( [(Gs[0],100 * np.random.rand()), (Gs[1],100 * np.random.rand())] )
    Mtest = np.array(Mtest, dtype = 'float')
    assert abs((np.linalg.det(Mtest) - 1) ) < 1e-3, 'M is not symplectic!!, det(M) = ' + str(np.linalg.det(Mtest))

    return [ [ M[0,1], M[2,3] ], Ms[:-1] ]


def solve(Ds, Ks, L, x_init, px_init, gamma):
    '''
    Solves for etc... with numQs quadrupoles, of which the first two have a variable strength, and fixed for the rest ...
    -----------------------------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------------------------
    Can read from json file triplet.json with the following information:
    {
     "Ds" : (list of floats) Drift lengths. Must be len(Ds) = numQs or len(Ds) = numQs + 1 if you want to have a drift in front of the first quad.
     "Ks" : (list of floats) Quad gradients. Must be len(Ks) = numQs - 2 .
     "L" : (float) Quad lengths. They all have the same length.
     "x_init" : (float) Initial x-coordinate.
     "px_init" : (float) Initlal transvese momentum.
     "gamma" : (float) Particle gamma.
    }
    Note that it is assumed that (x, px/p0) = (y, py/p0).
    '''
    # Set up problem
    print('There are ', 2 + len(Ks), 'quads and ', len(Ds), 'drifts')
    assert gamma > 1 ,'gamma of the particle must be gamma >= 1'
    p0 = np.sqrt( np.power(gamma,2.) - 1 )
    init = [x_init, px_init / p0]
    qmcRatio = 1758.82
    Gs = [L * Ksi * qmcRatio / p0 for Ksi in Ks]

    print('initial vector is', init)
    g0, g1 = sym.symbols('g0, g1')
    Gs = [g0,g1] + Gs
    [ms, Ms] = getProblem(Ds, Gs, init)
    print('The two equations are \n', ms[0], '\nand\n', ms[1])

    # Solve
    print('Solving...')
    sols = sym.solve_poly_system( [ ms[0], ms[1] ], g0, g1 )
    print('Solutions for g0 and g1 are', sols)
    # Substitue solutions
    solT = 0
    print('Taking solution', solT, '...')
    Gs[0] = Gs[0].subs( g0, sols[solT][0] )
    Gs[1] = Gs[1].subs( g1, sols[solT][1] )
    Ks = [Gsi / qmcRatio * p0 / L for Gsi in Gs]
    print('\nThe quad gradients have to be', Ks, '\n')

    # Assert zero transverse size after last drift
    M = np.linalg.multi_dot(Ms)
    init = np.array(init + init)
    final = M.dot(init)
    final = [el.subs([ (g0, sols[solT][0]), (g1, sols[solT][1]) ]) for el in final]
    final = [round(el,4) for el in final]
    print('We go from (x, px/p0, y, py/p0) = ', init)
    print('to', final)


# Run program
if __name__ == "__main__":
    for arg in sys.argv:
        if arg == '--help':
            help(solve)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()
            
    jsonFile = 'triplet.json'
    print('Reading from', jsonFile)
    with open(jsonFile) as json_file:
        kwargs = json.load(json_file)
        print(kwargs)
        solve(**kwargs)