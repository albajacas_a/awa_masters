# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: pyOPALtools (myenv)
#     language: python
#     name: pyopaltools
# ---

# + {"ein.hycell": false, "ein.tags": "worksheet-0", "slideshow": {"slide_type": "-"}}
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os 
import sys
sys.path.append('/home/arnau/documents/ETHZ/Semester_Project_Master_Thesis/pyOPALTools')
from opal.parser.H5Parser import H5Parser
from decimal import Decimal
from scipy.signal import find_peaks
import plotly.express as px
import plotly

########################
for_report = False
generic_fn = 'fullBeamline_ILS1=0:350:50_ILS2=0:350:50_ILS3=0:350:50/fullBeamline_ILS1=#_ILS2=#_ILS3=#/fullBeamline'

fs = 14
########################

if not for_report:
    from datetime import date
    today = date.today()
#     footnote = 'A.Albà, ' + str(today) + '\ndir: /' + (os.path.split(os.getcwd()))[1]
    footnote = 'A.Albà, ' + str(today)
    print(footnote)
    generic_title = 'Linacs and solenoids'
else:
    generic_title = " "
    footnote = " "


# -

# # Functions

# + {"ein.hycell": false, "ein.tags": "worksheet-0", "slideshow": {"slide_type": "-"}, "code_folding": [0, 36, 88, 96, 119, 156, 167]}
def get_ps_xy( fn, step, qx, qy, boolprint = False ):    
    ''' Get data to plot from file fn and given step. qx and qy are arrays of strings with data to plot
    eg ['z'], ['x', 'E'] '''
    h5p = H5Parser()
    h5p.parse( fn, boolprint )
    x = []
    for qi in qx:
        x.append( h5p.getStepDataset( dsetName = qi, step = step) )
    y  = []
    for qi in qy:
        if qi == 'E':
            # Get E
            E = []
            px = h5p.getStepDataset( dsetName = 'px', step = step)
            py = h5p.getStepDataset( dsetName = 'py', step = step)
            pz = h5p.getStepDataset( dsetName = 'pz', step = step)
            for i, pxi in enumerate(px):
                p2 = px[i]**2 + py[i]**2 + pz[i]**2  
                E.append( np.sqrt( 1 + p2 )*.511 )
            y.append( E )
        else:
            y.append( h5p.getStepDataset( dsetName = qi, step = step) )
    
    spos = h5p.getStepAttribute('SPOS', step = step)
    nSteps = h5p.getNSteps()
    if ( boolprint ):
        print(h5p.getStepDatasets(step = step))
        print('spos = ',spos)
    if len(x) == 1:
        x = x[0]
    if len(y) == 1:
        y = y[0]

    
    return [ x, y, spos[0], nSteps ]

def FT( x, y, cut = [], nbins = 100, boolplot = False, prominence = 0.0 ):
    # Sort
    idx   = np.argsort(x)
    x = np.array( x )[idx]
    y = np.array( y )[idx]
    # Trim data
    if len(cut) != 0:
        i = 0
        while( x[i] < cut[0] ):
            i += 1
        j = len(x) - 1   
        while( x[j] > cut[1] ):
            j -= 1    
        x = x[ i : j ]
        y = y[ i : j ]
    # Flatten data
    m = ( y[-1] - y[0] ) / ( x[-1] - x[0] )
    #y = [ y[i] - m * xi for i, xi in enumerate(x) ]
    y -= np.average(y)
    # Get data organised in bins
    x_bins = np.linspace( np.min(x), np.max(x), nbins )
    digitized = np.digitize( x, x_bins ) - 1 
    y = np.array(y)
    E_means = [ y[digitized == i].mean() for i in range(nbins) ]
    # Plot
    if boolplot:
        fig, ax = plt.subplots(1,2)
        fig.set_size_inches(15, 5)
        ax[0].scatter( x_bins, E_means, marker = '.', color = 'k' )
        ax[0].grid()
        ax[0].tick_params( axis = 'both', labelsize = fs)
        if len(cut) != 0:
            ax[0].set_xlim( left = cut[0], right = cut[1]  )
    # Fourier transform        
    dist = np.max(x) - np.min(x)
    amp = np.fft.fft(E_means)[:nbins//2] / nbins
    freq = np.arange( nbins//2 ) / dist
    # Find peak frequencies
    peaks, _ = find_peaks( np.abs(amp), prominence = prominence )
    amp_peaks = np.abs(amp)[peaks]
    peaks = peaks / dist
    if boolplot:
        for peak in peaks:
            ax[1].axvline( peak, color = 'r' )
        ax[1].plot( freq, np.abs(amp) )
        ax[1].grid()
        ax[1].tick_params( axis = 'both', labelsize = fs)
        ax[1].set_xlabel('freq [$m^-1$]')
        print('peaks at ', peaks)
    
    return [ freq, amp, peaks, amp_peaks ]

def fillIn( fn, rep = [], show = False ):
    newfn = fn
    for r in rep:
        newfn = newfn.replace( '#', str(int(r)), 1 )
    if show:
        print(newfn)
    return newfn

def getStats( fn, show = False ):
    fn = fn + '.stat'
    text = open( fn, 'r' )
    names = []
    units = []
    for line in text:
        if 'name' in line:
            ind1 = line.find('=')
            ind2 = line.find(',')
            name = line[ind1+1:ind2]
        if 'units' in line:
            ind1 = line.find('=')
            ind2 = line.find(',')
            unit = line[ind1+1:ind2]
            if unit == '1':
                unit = ' '
            names.append( str(name) + ' [' + str(unit) + ']' )
    for i in names:
        if show:
            print( i, names[i])
    return pd.read_csv( filepath_or_buffer = fn,
                       skiprows = 279, sep = '\s+', names = names)

def plotStats( stat, ax1, xax, ylax, yrax, colorL = 'r', colorR = 'b', show = False ):
    names = stat.columns
    x = np.array( stat[names[xax]] )
    labxax = names[xax] 
    # Left axis y
    y = np.array( stat[names[ylax]] )
    labyax = names[ylax]
    if 'emit' in labyax:
        y = np.log(y)
        labyax = 'log ' + labyax
        
    ax1.plot( x, y, color = colorL )
    ax1.set_xlabel(labxax, fontsize = fs)
    ax1.set_ylabel(labyax, fontsize = fs, color = colorL)
    ax1.tick_params( axis = 'y', labelsize = fs, labelcolor = colorL )
    ax1.tick_params( axis = 'x', labelsize = fs, labelcolor = 'k')

    plt.text( .75, -.25, footnote, transform=ax1.transAxes )
    plt.grid()
    # plt.xlim(left=.1)
    
    # Right axis y
    if yrax != -1:
        y = np.array( stat[names[yrax]] )
        labyax = names[yrax] 
        if 'emit' in labyax:
            y = np.log(y)
            labyax = 'log ' + labyax
        ax2 = ax1.twinx()
        ax2.plot( x, y, color = colorR )
        ax2.set_ylabel(labyax, fontsize = fs, color = colorR)
        ax2.tick_params( axis = 'y', labelsize = fs, labelcolor = colorR )
        if show:
            print(xax, names[xax], ', ',ylax, names[ylax], ', ', yrax, names[yrax])
    elif show:
        print(xax, names[xax], ', ',ylax, names[ylax])
        
def accept(x,y, limx = 0, cutoffy = 1, cutoffSlope = 0):
    x = np.array(x)
    y = np.array(y)
    if y[-1] > cutoffy:
        return 0
    while x[0] < limx:
        x = x[1:]
        y = y[1:]
    slope = (y[-1]-y[0]) / (x[-1]-x[0])
    return slope < cutoffSlope

def combine( ranges ):
    total_combs = []
    for r in ranges:
        n = len(r)
        n2 = np.power(n,2)
        combs = np.zeros( [np.power(n,3),3] )
        for i, c in enumerate(combs):
            combs[i][0] = int(r[int(i/n2)])
            combs[i][1] = r[int(i/n)%n]
            combs[i][2] = r[i%n]
        total_combs.append(combs)
    return np.concatenate(total_combs)
            
# combs = combine( [ [0,50,100,150,200], [250,300,350] ] )
combs = combine( [ [0,50,100,150,200], [250,300] ] )

# -

# # Code

# + {"ein.hycell": false, "ein.tags": "worksheet-0", "slideshow": {"slide_type": "-"}}
fn = fillIn( generic_fn, rep = [200,200,200], show = True)
stat = getStats( fn, show = False )

fig, ax1 = plt.subplots()
plotStats( stat, ax1, 1, 5, 35, 'r', 'b', show = True )
x = np.array(stat['s [m]'])
y = np.array(stat['rms_y [m]'])
ax1.plot( x, y, color = 'g' )
ax1.set_ylabel( 'rms [m]', color = 'k', fontsize = fs )
ax1.tick_params( axis = 'y', labelsize = fs, labelcolor = 'k' )
ax1.legend(['rms_x', 'rms_y'], bbox_to_anchor = [1.0, .8], fontsize = fs)
plt.title(generic_title + ', \n linac solenoids at I = 200 A', fontsize = fs)
plt.savefig('plots/rms_x.eps', bbox_inches='tight')
plt.show()
# -

stats = []
for c in combs:           
    fn = fillIn( generic_fn, rep = c, show = False)
    stat = getStats( fn, show = False )
    ILS = str(c)
    stat['ILS'] = [ILS]*stat.shape[0]
    stats.append(stat)
stats = pd.concat(stats)
names = stats.columns
fig = px.scatter(stats, x = names[1], y = names[5], color="ILS")
plotly.offline.plot(fig, filename = 'plots/all.html', auto_open=False)
fig.show()

# +
stats = []
for c in combs:           
    fn = fillIn( generic_fn, rep = c, show = False)
    stat = getStats( fn, show = False )
    if not accept(stat['s [m]'], stat['rms_x [m]'], cutoffy = 5e-3, limx = 10):
        continue
    ILS = str(c)
    stat['ILS'] = [ILS]*stat.shape[0]
    stats.append(stat)
stats = pd.concat(stats)

fig = px.scatter(stats, x = 's [m]', y = 'rms_x [m]', color="ILS")
plotly.offline.plot(fig, filename = 'plots/cutoff.html', auto_open=False)
fig.show()
