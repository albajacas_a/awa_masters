import numpy as np

def getCurrent (Q, length):
    return Q / length

def getKVector (lu):
    return 2 * np.pi / lu

def getGamma0 (gamma,K):
    return gamma / np.sqrt(1 + .5 * K**2)

def getGamma (E):
    return E / .511

def getFELparam ( gamma, K, lu, Q, length, sigr):
    g0 = getGamma0(gamma, K)
    ku = getKVector(lu)
    I = getCurrent(Q,length)
    fc = .9
    Ia = 17e3

    return 1 / g0 * (I / 2 / Ia * fc * K / 4 / ku / sigr)**(1/3)

def getLg (lu, rho):
    return lu / 4 / np.pi / np.sqrt(3) / rho



E = 50  # MeV
lu = 8.5e-2
K = 11.1
Q = 5e-9
length = 15e-12
sigr = 8e-3

g = getGamma(E)
rho = getFELparam(g, K, lu, Q, length, sigr)
Lg = getLg(lu, rho)

print('gamma = ', g,
      '\ngamma0 = ', getGamma0(g, K),
      '\nFEL parameter = ', rho,
      '\ngain length = ', Lg)
                  