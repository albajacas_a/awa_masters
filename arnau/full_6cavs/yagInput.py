#!/usr/bin/env python

import numpy as np
import json
import sys
import matplotlib.pyplot as plt
import pandas as pd
from opal.parser.H5Parser import H5Parser  # Requires pyOPALtools environment variable


def yagInput (inputFN):
    '''
    ------------------------------------------------------------------------------------------------
    Generates an input file for OPAL injected bunch from an h5 screen
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example:
    python3 yagInput.py --i tests/case2/DYAG12.h5
    python3 yagInput.py --help    
    '''
    
    ind = inputFN.rfind('.h5')
    if ind == -1:
        print('no .h5 file was found')
        exit()
    outputFN = inputFN[:ind] + '.tsv'
    print('Output is saved to', outputFN)
    
    print('Getting data from', inputFN)
    h5p = H5Parser()
    h5p.parse(inputFN, False )
    # print(h5p)
    x = h5p.getStepDataset( dsetName = 'x', step = 0)
    px = h5p.getStepDataset( dsetName = 'px', step = 0)
    y = h5p.getStepDataset( dsetName = 'y', step = 0)
    py = h5p.getStepDataset( dsetName = 'py', step = 0)
    z = -3e8 * h5p.getStepDataset( dsetName = 'time', step = 0)
    pz = h5p.getStepDataset( dsetName = 'pz', step = 0)

    z -= z.mean()

    spos = h5p.getStepAttribute('SPOS', step = 0)
    print('screen pos = ',spos)

    print('Saving to file...') 
    file = open( outputFN, mode = 'w' )  
    file.write(str(len(z)) + '\n')  # Number of particles needs to be first line in OPAL
    file.close()
    df = pd.DataFrame([x,px,y,py,z,pz])
    df = df.T
    df.to_csv( outputFN, sep = '\t', header = False, index = False, mode = 'a' )

# Run program
if __name__ == "__main__":
    for i, arg in enumerate(sys.argv):
        inputFN = ''
        print(arg)
        if arg == '--help':
            help(yagInput)
        elif arg == '--i':
            inputFN = sys.argv[i+1]
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()
        else:
            continue

        print('Using file ', inputFN)
        yagInput(inputFN)