#!/usr/bin/env python
'''
Generates a train of gaussian pulses along the longitudinal direction and saves it in "filename.csv" to be used as an OPAL input distribution.
'''

import numpy as np
import json
import matplotlib.pyplot as plt
import pandas as pd
import sys

def gauss( x, mu = 0.0, sig = 1.0 ):
    return np.exp( - ( x - mu )**2 / ( 2 * sig**2 ) )

def multiGauss( x, nPulse, sep, sig, start ):
    y = 0
    for i in range(nPulse):
        y += gauss( x, mu = start + sep * i, sig = sig )
    return y / nPulse

def xyUnif( R = 1.0 ):
    accept = 0
    while not accept:
        x = 1.1 * R * np.random.uniform(-1.0,1.0)
        y = 1.1 * R * np.random.uniform(-1.0,1.0)
        accept = (x**2 + y**2 <= R**2)
    return [x,y]

def generate_multi_gauss ( nPart = 1000, nPulse = 1, sep = 1e-12,
                           sigZ = 1e-12, sigR = 1e-3, E = 50, sigPz = 2, sigPr = .1, filename = 'multiGauss.csv', plot = False,
                           reduceK = 0.0):
    '''
    ------------------------------------------------------------------------------------------------
    Generates a train of gaussian pulses along the longitudinal direction and saves it to be used as an OPAL input distribution.
    Only works for electrons.
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Arguments obtained from json file 'multi_gauss.json'
    {
    	"nPart" : (integer),
    	"nPulse" : (integer) Number of gaussian pulses in the distribution,
        "sep" : (float, units: seconds or meters) Longitudinal spearation between pulses,
        "sigZ" : (float, units: seconds or meters) Longitudinal standard deviation of Gaussian,
    	"sigR" : (float, units: meters) Transverse radius of the uniform distribution
    	"E" : (float, units: MeV) Mean energy,
    	"sigPz" : (float, units: unitless (beta*gamma) ) Longitudinal momentum standard deviation,
    	"sigPr" : (float, units: unitless (beta*gamma) ) Transverse momentum momentum standard deviation,
    	"filename" : (string) File in which to store distribution,
    	"plot" : (boolean) Plot the distrbution or not
        "reduceK" : (doulble) K of the ficticious undulator that would squash the bunch and reduce gamma
    }
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example:
    python3 generate_multi_gauss.py
    python3 generate_multi_gauss.py --help    
    '''

    mc2 = .511  # For electrons
        
    # if reduceK > 0.0:
    #     gamma = E / mc2
    #     beta  = np.sqrt(1 - 1 / gamma**2)
    #     gamma0 = gamma / np.sqrt(1 + .5 * reduceK**2)
    #     beta0  = np.sqrt(1 - 1 / gamma0**2)
    #     ksi = beta0 / beta
    #     sep *= ksi
    #     sigZ *= ksi
    #     E = gamma0 * mc2
    #     sigPz *= gamma0 * beta0 / (gamma * beta)
    
    
    length = sep * ( nPulse - 1 )
    maxZ = length / 2 + 3 * sigZ
    nPart = int(nPart)
    nPulse = int(nPulse)
    
    x = []
    y = []
    z = []
    px = []
    py = []
    pz = []
    gamma = E / mc2
    meanPz = np.sqrt(gamma**2 - 1)

    j = 1
    for i in range(nPart):
        if i >= nPart * .05 * j :
            print(j * 5, 'percent of particles have been placed.')
            j += 1
        
        # Coordinates
        accept = 0
        while( not accept ):
            zi = ( 2 * np.random.uniform() - 1 ) * maxZ
            accept = np.random.uniform() < multiGauss( zi, nPulse, sep, sigZ, -.5 * length )
        z.append( zi )
        [xi,yi] = xyUnif( sigR )
        x.append( xi )
        y.append( yi )
        
        # Momentum
        px.append( sigPr * np.random.randn() )
        py.append( sigPr * np.random.randn() )
        pz.append( meanPz + sigPz * np.random.randn() )
        
    # Save distribution in file
    ## Write particle number in first line
    print('\nSaving particles in file...')
    file = open( filename, mode = 'w' )
    file.write( str(nPart) + '\n' )
    file.close()
    ## Write the distribution
    df = pd.DataFrame([x,px,y,py,z,pz])
    df = df.T
    df.to_csv( filename, sep = '\t', header = False, index = False, mode = 'a' )

    # Plot distribution
    if plot:
        print('\nPlot distribution')
        nbins = int( nPart / 20 )
        fs = 12
        fig, ax = plt.subplots( 2, 2 )
        fig.set_size_inches(15, 10)
        ax[0,0].hist2d( x, y, bins = nbins, cmin = 1 )
        ax[0,0].set_xlabel('$x$ [m]', fontsize = fs)
        ax[0,0].set_ylabel('$y$ [m]', fontsize = fs)
        
        ax[0,1].hist( z, bins = nbins )
        ax[0,1].set_xlabel('$z$ [m or s]', fontsize = fs)
        
        ax[1,0].hist2d( px, py, bins = nbins, cmin = 1 )
        ax[1,0].set_xlabel('$p_x$ [ ]', fontsize = fs)
        ax[1,0].set_ylabel('$p_y$ [ ]', fontsize = fs)
        
        ax[1,1].hist( pz, bins = nbins )
        ax[1,1].set_xlabel('$p_z$ [ ]', fontsize = fs)
        plt.show()

        
# Run program
if __name__ == "__main__":
    for arg in sys.argv:
        if arg == '--help':
            help(generate_multi_gauss)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()
            
    jsonFile = 'multi_gauss.json'
    print('Reading from', jsonFile)
    with open(jsonFile) as json_file:
        kwargs = json.load(json_file)
        print(kwargs)
        generate_multi_gauss(**kwargs)

