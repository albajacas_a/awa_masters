import numpy as np
import matplotlib.pyplot
import sys

def gauss (mu, sig, x):
    return np.exp(- (x - mu)**2 / 2 / sig**2)

def den (sep, npeaks, sig):
    peaks = []
    for i in range(npeaks):
        peaks.append(i * sep)
        
    peak = peaks[int(npeaks / 2)]
    valley = peak - sep / 2
    
    peakP = 0
    valleyP = 0
    for p in peaks:
        peakP += gauss(p, sig, peak)
        valleyP += gauss(p, sig, valley)

    return 1 - valleyP / peakP
                       

sig = .3 / 2.355
npeaks = int(sys.argv[1])
wantDen = float(sys.argv[2])

print('npeaks = ', npeaks)
print('wanted density = ', wantDen)

seps = np.linspace(0, sig * 12, num = 100)
print('Attempt separation range [', seps[0], ',', seps[-1], ']')

for i,s in enumerate(seps):
    if den(s, npeaks, sig) >= wantDen:
        x1 = s
        y1 = den(x1, npeaks, sig)
        print('Density with sep = ', x1, ' is den = ', y1 )
        if i == 0:
            sys.exit()
        print('Interpolating...')
        x0 = seps[i-1]
        y0 = den(x0, npeaks, sig)
        finalSep = seps[i-1] + (wantDen - y0) * (x1 - x0) / (y1 - y0)
        print('Density with sep = ', finalSep, ' is den = ', den(finalSep, npeaks, sig) )
        sys.exit()

print('required sep is out of range')
        