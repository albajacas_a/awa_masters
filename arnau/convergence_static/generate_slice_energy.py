import numpy as np
import json
import matplotlib.pyplot as plt
import pandas as pd
import sys
sys.path.append('/data/user/albajacas_a/pyOPALTools/')
import opal as opalTools
        
def generate_slice_energy (inputs, nbins = 10, edges = [-3.0e-12, 3.0e-12], plot = False ):
    '''
    ------------------------------------------------------------------------------------------------
    Generate slice energy and slice energy-spread
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example arguments obtained from json file 'generate_slice_energy.json'
    {
    "inputs" : ["tests/highRes_seed1"],
    "nbins" : 10,
    "edges" : [-3.0e-12, 3.0e-12],
    "plot" : 0
    }
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example:
    python3 generate_slice_energy.py
    python3 generate_slice_energy.py --help    
    '''
    outerEdges = edges
    for inputFN in inputs:
        ind = inputFN.rfind('/')
        outputFN = 'slice_energies/' + inputFN[ind+1:] + '.slice'
        print('Slice quantities will be saved in', outputFN)

        print('edges = ', outerEdges)
        edges = np.linspace(outerEdges[0], outerEdges[1], nbins+1)
    
        print('Getting data from', inputFN)
        h5p = opalTools.parser.H5Parser.H5Parser()
        h5p.parse(inputFN + '/DYAG4.h5', False )
        # print(h5p)
        t = h5p.getStepDataset( dsetName = 'time', step = 0)
        t = np.array(t)
        t -= t.mean()
        print('min, max t', np.min(t), np.max(t))
        # Get Energy
        p2 = np.array(h5p.getStepDataset( dsetName = 'px', step = 0))**2
        p2 += np.array(h5p.getStepDataset( dsetName = 'py', step = 0))**2
        p2 += np.array(h5p.getStepDataset( dsetName = 'pz', step = 0))**2
        mc2 = .511
        E = mc2 * np.sqrt(1 + p2)

        # Compute slice energy and slice energy spread
        avgE = np.zeros(nbins)
        avgE2 = np.zeros(nbins)
        N = np.zeros(nbins)
        for i in range(len(E)):
            ind = np.sum([t[i] > edges]) - 1
            if ind < 0 or ind >= nbins:
                continue
            avgE[ind] += E[i]
            avgE2[ind] += E[i]**2
            N[ind] += 1
        for i in range(nbins):
            avgE[i] /= N[i]
            avgE2[i] /= N[i]
        stdE = np.sqrt(avgE2 - avgE**2)
        print('average E = ', avgE.mean())
        
        # Save to file 
        print('Saving to file...') 
        file = open( outputFN, mode = 'w' )  
        file.write('Using file = ' +  inputFN + ', nbins = ' + str(nbins) + ', edges = ' + str(edges[0]) + ', ' + str(edges[-1]) + '\n' ) 
        file.write('avgE stdE\n')
        for i in range(nbins):
            file.write(str(avgE[i]) + ' ' +  str(stdE[i]) + '\n')
        file.close()                
            
        spos = h5p.getStepAttribute('SPOS', step = 0)
        print('screen pos = ',spos)
        
        # Plot distribution
        if plot:
            print('Plotting...')
            fig, ax = plt.subplots()
            ax.scatter(edges[:-1], avgE)
            ax.scatter(edges[:-1], stdeE)
            ax.legend(['avg E', 'std E'])
            ax.set_xlabel('t')
            ax.set_ylabel('E [MeV]')

# Run program
if __name__ == "__main__":
    jsonFile = 'generate_slice_energy.json'
    for arg in sys.argv:
        if arg == '--help':
            help(generate_slice_energy)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()            

    print('Reading from', jsonFile)
    with open(jsonFile) as json_file:
        kwargs = json.load(json_file)
        print(kwargs)
        generate_slice_energy(**kwargs)

