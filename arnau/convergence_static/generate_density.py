#!/usr/bin/env python
'''
Generates a density plot from a screen
'''

import numpy as np
import json
import sys
import matplotlib.pyplot as plt
import pandas as pd
from opal.parser.H5Parser import H5Parser  # Requires pyOPALtools environment


def generate_density (inputs, nbins = 32, plot = False ):
    '''
    ------------------------------------------------------------------------------------------------
    Generate density plot with nbins x nbins x nbins
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example arguments obtained from json file 'generate_density.json'
    {
    "inputs" : ["tests/highRes_seed1"],
    "nbins" : (optional, default is 32) 32,
    "plot" : (optional, default is False) 0
    }
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example:
    python3 generate_density.py
    python3 generate_density.py --help    
    '''
    for inputFN in inputs:
        ind = inputFN.rfind('/')
        outputFN = 'densities/' + inputFN[ind+1:] + '.den'
        print('Density will be saved in', outputFN)
        nbins = int(nbins)
    
        print('Getting data from', inputFN)
        h5p = H5Parser()
        h5p.parse(inputFN + '/DYAG4.h5', False )
        # print(h5p)
        t = h5p.getStepDataset( dsetName = 'time', step = 0)
        pz = h5p.getStepDataset( dsetName = 'pz', step = 0)
        spos = h5p.getStepAttribute('SPOS', step = 0)
        print('screen pos = ',spos)

        t = np.array(t)
        t -= t.mean()
        pz = np.array(pz)
                
        print('Making histogram...')
        particles = np.array([t, pz]).T
        print('Particle array has shape', particles.shape)
        [hist, edges] = np.histogramdd(particles, bins = nbins, density = True)
        del t
        del pz
        print('Histogram array has shape', hist.shape)

        # Save density in file 
        print('Saving to file...') 
        file = open( outputFN, mode = 'w' )  
        file.write('Using file = ' +  inputFN + ', and nbins = ' + str(nbins) + '\n' ) 
        file.write(str(edges[0][0]) + ' ' + str(edges[0][-1]) + ' ' + str(edges[0].size) + '\n')
        file.write(str(edges[1][0]) + ' ' + str(edges[1][-1]) + ' ' + str(edges[1].size) + '\n')
        for i in range(nbins):
            for j in range(nbins):
                file.write(str(hist[i,j]) + '\n')
        file.close()
    
    # Plot distribution
    # if plot:
    #     print('Plotting...')
    #     fig, ax = plt.subplots()
    #     ax.hist2d(t, pz, bins = nbins, density = True)
    #     ax.set_xlabel('t')
    #     ax.set_ylabel('pz')

# Run program
if __name__ == "__main__":
    jsonFile = 'generate_density.json'
    for arg in sys.argv:
        if arg == '--help':
            help(generate_density)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()            

    print('Reading from', jsonFile)
    with open(jsonFile) as json_file:
        kwargs = json.load(json_file)
        print(kwargs)
        generate_density(**kwargs)

