#!/usr/bin/env python
'''
Generates a density plot from a screen
'''

import numpy as np
import json
import matplotlib.pyplot as plt
import pandas as pd
import os
import sys

def generate_quants ():
    '''
    ------------------------------------------------------------------------------------------------
    Generate quants

    Example:
    python3 generate_quants.py
    python3 generate_quants.py --help    
    '''

    quants = pd.read_csv('quants.tsv', sep=' ')
    params = pd.read_csv('cases.txt', skiprows = 1, comment  = '#', sep = ' ')
    params = params[~params['fname'].isin(quants['fname'])]
    params = params.reset_index()

    for ind, row in params.iterrows():
        inputFN = 'tests/' + row['fname'] + '/fullBeamline.stat'
        print('Getting data', inputFN)
        text = open( inputFN, 'r' )
        names = []
        for line in text:
            if 'name' in line:
                ind1 = line.find('=')
                ind2 = line.find(',')
                name = line[ind1+1:ind2]
                names.append( name )
        names = names[3:]
        stat = pd.read_csv( filepath_or_buffer = inputFN, skiprows = 279, sep = '\s+', names = names)
        last = len(stat['energy']) - 1
        E = float(stat['energy'][last])
        dE = float(stat['dE'][last])
        quants = quants.append({'fname' : row['fname'], 'E' : E,
                                'dE' : dE}, ignore_index = True)
        print('position = ', stat['s'][last], ', E = ', E, ', dE = ', dE)
    quants.to_csv('quants.tsv', sep = ' ', index = False)

    
# Run program
if __name__ == "__main__":
    for arg in sys.argv:
        if arg == '--help':
            help(generate_quants)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()            
    generate_quants()

