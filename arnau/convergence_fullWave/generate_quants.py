#!/usr/bin/env python
'''
Generates a density plot from a screen
'''

import numpy as np
import json
import matplotlib.pyplot as plt
import pandas as pd
import os
import sys
import sys
sys.path.append('/data/user/albajacas_a/plotMithraPy')
import plotMiFuncs as pmf


def generate_quants ():
    '''
    ------------------------------------------------------------------------------------------------
    Generate quants

    Example:
    python3 generate_quants.py
    python3 generate_quants.py --help    
    '''

    quants = pd.read_csv('quants.tsv', sep=' ')
    params = pd.read_csv('cases.txt', skiprows = 1, comment  = '#', sep = ' ')
    params = params[~params['fname'].isin(quants['fname'])]
    params = params.reset_index()

    for ind, row in params.iterrows():
        inputFN = 'tests/' + row['fname']
        print('Getting data', row['fname'], '...')
        [df, screens] = pmf.importScreen( inputFN + '/bunch-screen/bunch-p#-screen#.txt', show = True )
        px = np.array(df['px'])
        py = np.array(df['py'])
        pz = np.array(df['pz'])
        df = pd.DataFrame()  # Empty the dataframe
        mc2 = .511
        E = []
        for i in range(len(px)):
            E.append(mc2 * np.sqrt(1 + px[i]**2 + py[i]**2 + pz[i]**2))
        E = np.array(E)
        quants = quants.append({'fname' : row['fname'], 'E' : np.mean(E),
                                'dE' : np.std(E)}, ignore_index = True)

    quants.to_csv('quants.tsv', sep = ' ', index = False)

    
# Run program
if __name__ == "__main__":
    for arg in sys.argv:
        if arg == '--help':
            help(generate_quants)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()            
    generate_quants()

