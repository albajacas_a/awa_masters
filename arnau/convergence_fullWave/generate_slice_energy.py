import numpy as np
import json
import os
import sys
import matplotlib.pyplot as plt
import pandas as pd

######################################################################
######################################################################
# I COPIED THIS FROM PLOTMITHRA FUNCS BECAUSE THE NODES CANNOT IMPORT FNCITONS I DON?T UNDERSTANCD
def importScreenXY( fname, index_screens = [], show = False, pNames = [], xquant = 't', yquant = 'E', index_screen = 0, reduce_factor = 1 ):
    ''' 
    Returns x and y of the bunch screen data
    -fname : (String) Filename to read data from. Put # instead of numbers. eg tests/test2/bunch-screen/bunch-p#-screen#.txt
    -index_screens : (list of ints) Indices of screens to import. By default it will import all screens.
    -xquant : (string) Quantity you want (from pNames list or E for energy)
    -yquant : (string) Quantity you want
    -index_screen : (unsigned int) index of screen to get data from
    -reduce_factor : (double) proportion of random values to ignore in order to reduce computational memory
    -show  : (Boolean) Print info
    '''
    dirname = fname[:fname.rfind('/')]
    if len(pNames) == 0:
        pNames = ['q', 'x', 'y', 't', 'px', 'py', 'pz']
    if show:
        print( 'columns = ', pNames )

    # Get number of processors and screens
    nums = 0
    nump = 0
    for name in os.listdir( dirname ):
        ind1 = name.find('-')
        ind2 = name.rfind('-')
        nup = int(name[ ind1 + len('-p') : ind2 ])
        nus = int(name[ ind2 + len('-screen') : -len('.txt') ])
        if nup > nump:
            nump = nup
        if nus > nums:
            nums = nus
    nump += 1
    nums += 1
    if show:
        print( 'Number of screens = ', str(nums), ', number of processors = ', str(nump) )
    
    for i,ind in enumerate(index_screens): # such that -1 indexing works
        if ind < 0:
            index_screens[i] = nums + ind
    if index_screen < 0:
        index_screen += nums
    x = np.empty(0)
    y = np.empty(0)
    # Get the data
    posp = fname.find('#')
    poss = fname.rfind('#')
    data = []
    screenPos = []
    for s in range(nums):
        if (not s in index_screens) and len(index_screens) > 0:
            continue
        if s != index_screen:
            continue
        sname = fname[:poss] + str( s ) + fname[poss+1:]
        if show:
            print(s, sname)
        for p in range(nump):
            pname = sname[:posp] + str(p) + sname[posp+1:]
            with open(pname) as f:
                first_line = f.readline()
                ind = first_line.find('=')
                if ind != -1:
                    spos = float( first_line[ ind + 1:] )
                else:
                    spos = s
            if not spos in screenPos:
                screenPos.append(spos)
            if show:
                print('Reading', pname)
            df = pd.read_csv(pname, sep='\t', skiprows = 1, header = None, names = pNames)
            if reduce_factor > 1:
                d = []
                for i in range(df.shape[0]):
                    if np.random.rand() > 1 / reduce_factor:
                        d.append(i)
                df = df.drop(d)
            x = np.append(x,np.array(df[xquant]))
            if yquant == 'E':
                mc2 = .511 * 1e6  # eV/c2 electron mass
                px = np.array(df['px'])
                py = np.array(df['py'])
                pz = np.array(df['pz'])
                E = []
                for i, bg in enumerate( zip( px, py, pz) ):
                    g_i = np.sqrt( 1 + np.inner(bg,bg) )  # Get gamma of particle
                    E.append( mc2 * g_i )
                y = np.append(y,np.array(E))
            elif yquant == 'none':
                y = np.empty(0)
            else:
                y = np.append(y,np.array(df[yquant]))
    screenPos.sort()
    if show:
        print('Screens at ', screenPos)

    return [x,y]
##################################################################################
##################################################################################

        
def generate_slice_energy (inputs, nbins = 10, edges = [-3.0e-12, 3.0e-12], plot = False ):
    '''
    ------------------------------------------------------------------------------------------------
    Generate slice energy and slice energy-spread
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example arguments obtained from json file 'generate_slice_energy.json'
    {
    "inputs" : ["tests/highRes_seed1"],
    "nbins" : 10,
    "edges" : [-3.0e-12, 3.0e-12],
    "plot" : 0
    }
    ------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
    Example:
    python3 generate_slice_energy.py
    python3 generate_slice_energy.py --help    
    '''
    outerEdges = edges
    for inputFN in inputs:
        ind = inputFN.rfind('/')
        outputFN = 'slice_energies/' + inputFN[ind+1:] + '.slice'
        print('Slice quantities will be saved in', outputFN)

        print('edges = ', outerEdges)
        edges = np.linspace(outerEdges[0], outerEdges[1], nbins+1)
    
        print('Getting data from', inputFN)
        [t,E] = importScreenXY( inputFN + '/bunch-screen/bunch-p#-screen#.txt', show = True, index_screen = -1)
        t -= t.mean()
        E *= 1e-6
        print('min, max t', np.min(t), np.max(t))

        # Compute slice energy and slice energy spread
        avgE = np.zeros(nbins)
        avgE2 = np.zeros(nbins)
        N = np.zeros(nbins)
        for i in range(len(E)):
            ind = np.sum([t[i] > edges]) - 1
            if ind < 0 or ind >= nbins:
                continue
            avgE[ind] += E[i]
            avgE2[ind] += E[i]**2
            N[ind] += 1
        for i in range(nbins):
            avgE[i] /= N[i]
            avgE2[i] /= N[i]
        stdE = np.sqrt(avgE2 - avgE**2)
        print('average E = ', avgE.mean())
        
        # Save to file 
        print('Saving to file...') 
        file = open( outputFN, mode = 'w' )  
        file.write('Using file = ' +  inputFN + ', nbins = ' + str(nbins) + ', edges = ' + str(edges[0]) + ', ' + str(edges[-1]) + '\n' ) 
        file.write('avgE stdE\n')
        for i in range(nbins):
            file.write(str(avgE[i]) + ' ' +  str(stdE[i]) + '\n')
        file.close()                
        
        # Plot distribution
        if plot:
            print('Plotting...')
            fig, ax = plt.subplots()
            ax.scatter(edges[:-1], avgE)
            ax.scatter(edges[:-1], stdeE)
            ax.legend(['avg E', 'std E'])
            ax.set_xlabel('t')
            ax.set_ylabel('E [MeV]')

# Run program
if __name__ == "__main__":
    jsonFile = 'generate_slice_energy.json'
    for arg in sys.argv:
        if arg == '--help':
            help(generate_slice_energy)
        elif arg.startswith('-'):
            print(arg, 'is not an option.')
            exit()            

    print('Reading from', jsonFile)
    with open(jsonFile) as json_file:
        kwargs = json.load(json_file)
        print(kwargs)
        generate_slice_energy(**kwargs)

