import matplotlib.pyplot as plt
import numpy as np

lr = 0.01

pos = np.random.rand(100)
# pos2 = 
xs = np.linspace(0,1,100)
def totsin(x, pos, lr):
    return np.sum([np.sin(2*np.pi*(x-p)/lr) for p in pos])
    

fig,ax = plt.subplots(figsize = (5,8))
ax.plot(pos, np.ones(len(pos)), "o")
ax.plot(xs, [totsin(x,pos,lr) for x in xs])
plt.show()