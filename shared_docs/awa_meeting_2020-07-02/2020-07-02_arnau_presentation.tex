%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer Presentation
% LaTeX Template
% Version 1.0 (10/11/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------

\documentclass[8pt]{beamer}

\usepackage{xcolor}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{siunitx}
\usepackage{diagbox}
\usepackage{multicol}
\usepackage{appendixnumberbeamer}
\usepackage[export]{adjustbox}
\usepackage{enumerate}
\usepackage{enumitem}

\makeatletter
\newcommand*{\centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother

\setbeamertemplate{navigation symbols}{}%remove navigation symbols
\usetheme{Madrid}

%\definecolor{lightgray}{gray}{0.9}
%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[AWA Wiggler Modelling]{AWA Wiggler Modelling} % The short title appears at the bottom of every slide, the full title is only on the title page

\author{Arnau Alb\`a} % Your name
\institute[PSI] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Paul Scherrer Institute  \\ % Your institution for the title page
}
\date{\today} % Date, can be changed to a custom date

\begin{document}
\setbeamercolor{background canvas}{bg=white}  % White background
%% \begin{frame}
%% \titlepage % Print the title page as the first slide
%% \end{frame}

%% \begin{frame}
%% \frametitle{Overview} % Table of contents slide, comment this block out to remove it
%% \tableofcontents % Throughout your presentation, if you choose to use \section{} and \subsection{} commands, these will automatically be printed on this slide as an overview of your presentation
%% \end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\begin{frame}
  \frametitle{Discussion Points}
  \begin{itemize}[topsep = 4pt, label = \textbullet, itemsep = 4pt]
  \item Decide bunch parameters
  \item Agree on S2E simulations
  \item Aim of the paper (what if no experiment?)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Parameters and Theory}
  Quantities from theory:
  \begin{itemize}[topsep = 4pt, label = \textbullet, itemsep = 4pt]
  \item Diffraction size $\sigma_\text{diff} = \sqrt{\frac{\lambda_w}{2}\sigma_z}$, with $\lambda_w = 85$ mm. If $\sigma_r < \sigma_\text{diff}$ there is transverse coherence and all particles in a slice emit CSR in phase, if  $\sigma_r > \sigma_\text{diff}$ CSR is negligible.

  \item Resonant wavelength $\lambda_r = \frac{\lambda_w}{\gamma^2}\left(1 + \frac{K^2}{2}\right)$. If $\sigma_z < \frac{\lambda_r}{2\pi}$ there is longitudinal coherence, otherwise if $\sigma_z > \frac{\lambda_r}{2\pi}$ particles emit CSR at different phases, not coherently.
    
  \item Space-charge field size $\sigma_{SC} = \frac{\gamma}{\sqrt{1 + \frac{K^2}{2}}}\sigma_z$. If $\sigma_r \ll \sigma_{SC}$ the electric field becomes negligible at distances larger than $\sigma_{SC}$ from the bunch, thus we require $\sigma_{SC} < a$, where $a = 10$ mm is the vacuum tube size, in order to avoid effects from the metallic surroundings.
  \end{itemize}
  
  \begin{figure}
    \includegraphics[width=.43\textwidth]{img/sizeParameters.png}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{Parameters and Theory}
  Available parameters when beam reaches the wiggler at $\sim28$ m:
  \begin{itemize}[topsep = 4pt, label = \textbullet, itemsep = 4pt]
  \item Gaussian with FWHM = 300 fs,
  \item Flattop with FWHM = 420, 800, 1600, 3200, 6400 fs
  \item $\sigma_r$ = .1, .2, ..., 1.5 mm (limited by $\sigma_r\sim \frac{10 \text{mm}}{6}$)
  \end{itemize}
  Note: \textbf{In all cases we expect stronger induced energy-spread in a wiggler than in a drift!}\\
  \vspace{8mm}
  \begin{multicols}{2}
  Initial conditions in following plots:
  \begin{itemize}[topsep = 4pt, label = \textbullet, itemsep = 4pt]
  \item Mono-energetic beam at $E = 44$ MeV, $\sigma_E = 0$
  \item Distribution is a superposition of Gaussians of FWHM = 300 fs, separated by 230 fs
  \item Total charge $Q = 300$ pC (we can probably do higher charges for the longer bunches, and get stronger energy modulation)
  \end{itemize}
  
  \begin{figure}
    \includegraphics[width=.43\textwidth]{img/initialDistExample.png}
    \caption{Example flattop formed by superposition of 8 Gaussians.}
  \end{figure}
  \end{multicols}
\end{frame}

\begin{frame}
  \frametitle{Parameters Used in Simulations}
  In the following simulations we shall see bunches with FWHM $= 300, 420, 1600$ fs, and $\sigma_r=.1-1.5$ mm.
  These are shown as red ellipses in the following plot.
  \begin{figure}
    \includegraphics[width=.53\textwidth]{img/sizeParameters_zoom.png}
  \end{figure}
  We will see the longitudinal phase-space of the bunch after the wiggler, and compare it to the same bunch after a 1 metre drift.
\end{frame}

\begin{frame}
  \frametitle{Phase-Space after Wiggler, FWHM = 300fs, $\sigma_r = 1-1500$ \si{\micro\metre}}
  \begin{figure}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_300fs_part1.png}
    \hspace{8pt}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_300fs_part2.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Phase-Space after Wiggler, FWHM = 420fs, $\sigma_r = 1-1500$ \si{\micro\metre}}
  \begin{figure}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_420fs_part1.png}
    \hspace{8pt}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_420fs_part2.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Phase-Space after a Wiggler or a Drift, FWHM = 420fs, $\sigma_r = 1-1500$ \si{\micro\metre}}
  \begin{figure}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_420fs_part1.png}
    \hspace{8pt}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/drift_FWHM_420fs_part1.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Phase-Space after Wiggler, FWHM = 1600fs, $\sigma_r = 1-1500$ \si{\micro\metre}}
  \begin{figure}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_1600fs_part1.png}
    \hspace{8pt}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_1600fs_part2.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Phase-Space after a Wiggler or a Drift, FWHM = 1600fs, $\sigma_r = 1-1500$ \si{\micro\metre}}
  \begin{figure}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/FWHM_1600fs_part2.png}
    \hspace{8pt}
    \includegraphics[width=.41\textwidth, trim=0 0 0 0, clip]{img/drift_FWHM_1600fs_part2.png}
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Energy Difference $\Delta E$ after a Wiggler or After a Drift}
  \begin{figure}
    \includegraphics[width=.65\textwidth, trim=0 0 0 0, clip]{img/dE_sigr.png}
  \end{figure}
\end{frame}


\end{document}