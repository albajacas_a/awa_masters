import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt

def getL(l, units):
    if units == 'cm':
        return float(l)/100
    elif units == 'inch':
        sums = l.split('+')
        for i, s in enumerate(sums):
            nums = s.split('/')
            nums.append('1')
            sums[i] = float(nums[0]) / float(nums[1])
        return np.sum(sums) * .0254
    else:
        print('Unknown units', units)
        sys.exit()


#######################################################
# Main
######################################################

filename = str(sys.argv[1])
print('Reading element posiitons from ', filename + '.txt')

# Get distances
print('Getting data...')
file1 = open(filename + '.txt', 'r') 
lines = file1.readlines()
ind = lines[0].find('=')
prevDist = float(lines[0][ind + 1:])  # Firt line of file is "init position = 5.0"
print('Initial distance is = ', prevDist)

# Sum up
data = pd.read_csv( filename + '.txt', skiprows = 1, sep = ',', names = ['l','units','element'] )
position = []
for i,row in data.iterrows():
    l = getL(row['l'], row['units'])
    position.append(prevDist + l)
    if not row['element']:
        prevDist += l
data['position'] = position

# Make labels
print('Making labels')
fig, ax = plt.subplots()
fig.set_size_inches(100, 55)
img = plt.imread(filename + '.png')
#ax.imshow( img, extent = [data['position'].iloc[-1], 0, 0,1] )
ax.set_xlim([ data['position'].iloc[-1], data['position'].iloc[0] ])
ax.set_ylim([-2,2])
for i,row in data.iterrows():
    if row['element']:
        color = 'r'
    else:
        color = 'b'
    if i%2:
        ymin = .51
        textAngle = 50
        textPos = .9
    else:
        ymin = .24
        textAngle = -50
        textPos = -1.3
    ax.axvline(row['position'], ymin = ymin, ymax = ymin + .2, color = color)
    ax.text( row['position'], textPos, '{:0.2f}'.format(row['position'])+'m', rotation = textAngle, fontsize = 8 )

ax.axis('off')
# ax.text( 9., -1., 'nb: element centres (red) are correct and are used in OPAL, \nbut the element size is not to scale in this drawing,\nso element edges (blue) are different in OPAL and this drawing.', fontsize = 10)
plt.show()
fig.savefig(filename + '_labels.png', bbox_inches = 'tight', transparent = True)