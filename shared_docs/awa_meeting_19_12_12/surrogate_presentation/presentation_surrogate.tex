\documentclass[handout]{beamer}

\usepackage{booktabs}
\usepackage{hyperref}

\usetheme{metropolis}

\author{Renato Bellotti}
\title{Building a surrogate model for OPAL}

\begin{document}
\maketitle

\begin{frame}{Problem}
\textbf{\large So far: Find beam characteristics by running OPAL}

\begin{minipage}[t]{0.55\textwidth}
\textbf{Design variables (DVAR)}
\begin{itemize}
	\item Current in buck focusing solenoid IBF
	\item Current in matching solenoid IM
	\item Gun phase GPHASE
	\item Solenoids between cavities\\
		ILS1, ILS2, ILS3
	\item bunch charge
	\item Cavity voltage
\end{itemize}
\end{minipage} \pause
\begin{minipage}[c]{0.1\textwidth}
\vspace{2cm}
\textbf{\large{$\stackrel{\textrm{OPAL}}{\Rightarrow}$}}
\end{minipage} \pause
\begin{minipage}[t]{0.25\textwidth}
\textbf{Quantities of interest (QOI)}
\begin{itemize}
	\item $\sigma_x, \sigma_y, \sigma_s$
	\item $\epsilon_x, \epsilon_y, \epsilon_s$
	\item $E, \Delta E$
	\item corr(x, $p_x$), corr(y, $p_y$), corr(s, $p_s$)
\end{itemize}
\end{minipage} \pause
\vspace{0.2cm}\\
\textcolor{red}{\textbf{$\ominus$ Expensive to calculate!}}
\end{frame}

\begin{frame}{Solution: Surrogate}
\textbf{\Large Idea: Approximate OPAL with neural network} \pause

\textbf{\large Time needed for 1 evaluation:}
\begin{itemize}
	\item OPAL: \textcolor{red}{$\mathcal{O}(\textrm{4-5~min})$} on 4 cores
	\item Surrogate: \textcolor{blue}{$\mathcal{O}(\textrm{100~ms})$} on 4 cores
\end{itemize} \pause
\textbf{\large Time needed for an optimisation:\\
(200 generations, 100 evaluations each)}
\begin{itemize}
	\item OPAL: \textcolor{red}{$\mathcal{O}(\textrm{1 day})$} (on hundreds of cores)
	\item Surrogate: \textcolor{blue}{$\mathcal{O}(\textrm{10~s})$} (on ~4 cores)
\end{itemize}
\end{frame}

\begin{frame}{Overview}
Steps to build a surrogate model for OPAL:
\begin{enumerate}
	\item \textbf{\large Generate the dataset: Run OPAL many times}
		\begin{itemize}
			\item \textbf{Training set:} {\small(4k runs)}\\
				Used to build the model
			\item \textbf{Test set:} {\small(1k runs)}\\
				Used to estimate the quality of the predictions
		\end{itemize} \pause
	\item \textbf{\large Clean the dataset}\\
		Needed for physical reasons (e.~g. $\sigma \stackrel{!}{<}$ pipe) \pause
	\item \textbf{\large Train the model} \pause
	\item \textbf{\large Assess performance}
\end{enumerate}
\end{frame}

\section{Dataset construction}

\begin{frame}{Run OPAL many times}
Sample design variable configurations randomly. (latin hypercube)
		
Ranges:

\vspace{0.1cm}
		
\resizebox{\textwidth}{!}{
\begin{tabular}{lllllllll}
	\toprule
	Bound & IBF [A] & IM [A] & GPHASE [$^\circ$] & ILS1 [A] & ILS2 [A] & ILS3 [A] & Bunch charge [nC] & Cavity voltage [MV]\\
	\midrule
	Lower bound & 200 & 210 & -25 & 50 & 100 & 150 & 1 & 12\\
	Upper bound & 450 & 260 & 0 & 250 & 200 & 200 & 5 & 25\\
	\bottomrule
\end{tabular}} \pause

\vspace{0.4cm}

Evaluate each configuration with OPAL
\end{frame}

\begin{frame}{Interpolate and clean up}
\only<1>{\includegraphics[width=\textwidth]{img/workflow_diagram_1.pdf}} \pause
\only<2>{\includegraphics[width=\textwidth]{img/workflow_diagram_2.pdf}}
\only<3>{\includegraphics[width=\textwidth]{img/workflow_diagram_3.pdf}}
\only<4>{\includegraphics[width=\textwidth]{img/workflow_diagram_4.pdf}}
\end{frame}

\section{Surrogate model}

\begin{frame}{Surrogate model}
\begin{center}
	\includegraphics[width=\textwidth]{img/network_in_out.png}
\end{center}
\end{frame}

\begin{frame}{Model architecture}
\begin{minipage}{0.45\paperwidth}
	\includegraphics[width=\textwidth]{img/network_annotated.png}\\
	\vspace{0.8cm}\\
	Activation: ReLU\\
	\includegraphics[width=0.8\textwidth]{img/relu.png}
\end{minipage}
\begin{minipage}{0.35\paperwidth}
	\begin{itemize}
		\item \textbf{Batch size:}\\
			128
		\item \textbf{Initial learning rate:}\\
			0.001
		\item \textbf{Optimiser:}\\
			Adam
		\item \textbf{Epochs:}\\
			700
		\item \textbf{Trainable parameters:}\\
			1'180'602
	\end{itemize}
\end{minipage}
\end{frame}

%\begin{frame}{Training}
%\textbf{\huge Input:}

%{\large X: design variable matrix scaled to range $[-1, 1]$}\\
%%{\large $y_{true}$: true QOI matrix scaled to range $[-1, 1]$}\\
%\vspace{0.5cm}
%\textbf{\huge Output:}

%$y_{pred}$: predicted QOI matrix (entries are in $[-1, 1]$)\\
%\vspace{1cm}
%\textbf{\huge Training:}

%\large Minimise
%\begin{equation*}
%	|y_{true} - y_{pred}|
%\end{equation*}
%using stochastic gradient descent (adam).
%\end{frame}

\begin{frame}{Surrogate Model: Input and Output}
\begin{itemize}
	\item The model can take a single DVAR configuration or many of them, and predict the corresponding QOIs. \pause
	\item Format of the input and output matrices:\\
		\vspace{0.5cm}
		\includegraphics[width=0.5\textwidth]{img/table.png}
\end{itemize}
\end{frame}

\section{Assess performance}

\begin{frame}{Surrogate model: Performance evaluation}
\textbf{\huge Evaluation workflow} \pause
\begin{enumerate}
	\item Evaluate the model for the design variables in the test set:
		\begin{center}
			$y_{pred} = m(X_{test})$
		\end{center} \pause
	\item Calculate the MAPE for each QOI and each sample in the test set, i.~e.~
		\begin{center}
			MAPE = $\frac{y_{test} - y_{pred}}{y_{test}} \cdot 100$ element-wise
		\end{center} \pause
		\textbf{Remember: This is a matrix!}\\
		{\tiny (each row contains the rel. error of all QOIs corresponding to a single DVAR configuration)}\\
		\includegraphics[width=0.3\textwidth]{img/table.png} \pause
	\item The rel. errors follow a distribution. This distribution is what we are visualising.
\end{enumerate}
\end{frame}

\begin{frame}{Refresher: Descriptive statistics}
\begin{itemize}
	\item When we don't know analytically how errors are distributed, we can still describe what we have measured. \pause
	\item Popular choice: Boxplots
		\begin{center}
			\includegraphics[width=\textwidth]{img/boxplot.png}\\
			\textcolor{gray}{\tiny \url{https://towardsdatascience.com/understanding-boxplots-5e2df7bcbd51}}
		\end{center}
\end{itemize}
\end{frame}

\begin{frame}{Surrogate Model: Performance I}
	\begin{center}
		\includegraphics[width=0.75\textwidth]{img/surrogate/boxplot.png}
	\end{center}
\end{frame}

\begin{frame}{Surrogate Model: Performance II}
Full picture: The distribution (kernel density estimation)

\begin{center}
	\includegraphics[width=\textwidth]{img/surrogate/emittance_distributions.png}
\end{center}
\end{frame}

\begin{frame}{Surrogate Model: Performance III}
Another visualisation:\\
Mean $\pm$ 1~standard deviation over the rows of the MAPE matrix

\begin{center}
	\includegraphics[width=\textwidth]{img/surrogate/emittance.png}
\end{center}
\end{frame}

\begin{frame}{Surrogate Model: Performance IV}
The model error distribution (especially the outliers!) depends strongly on the longitudinal position. \pause

Visualising the dependence of the error distribution on the longitudinal position:
\begin{center}
	Video
\end{center}
\end{frame}

\begin{frame}{Surrogate Model: Performance V}
Compare individual predictions to the ground truth (OPAL): \pause

\begin{center}
	\href{https://awa-wiggler-surrogate-vs-truth.herokuapp.com/}{Dashboard}
\end{center}
\end{frame}

\begin{frame}{Image sources}
\begin{itemize}
	\item \url{https://cdn-media-1.freecodecamp.org/images/1*1mpE6fsq5LNxH31xeTWi5w.jpeg}
	\item \url{https://medium.com/@danqing/a-practical-guide-to-relu-b83ca804f1f7}
	\item \url{https://towardsdatascience.com/understanding-boxplots-5e2df7bcbd51}
\end{itemize}

\end{frame}

\end{document}