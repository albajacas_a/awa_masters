# awa_masters

Shared repo for Master's Thesis


# Timing of a a 4 core run:

	    Problem size:
            MT: 16
            MX: 8
            MY: 8
            NP: 10000

                  num Nodes   CPU tot  Wall tot
===============================================
mainTimer...........      4     43.66     44.22

                  num Nodes   CPU max  Wall max   CPU min  Wall min   CPU avg  Wall avg
=======================================================================================
mainTimer...........      4     43.66     44.22     41.27     44.22     43.03     44.22
Binaryrepart........      4      0.03   0.01962      0.02   0.01821     0.025   0.01926
Boundingbox.........      4       1.4     1.214      1.17     1.171     1.255     1.191
Boundingbox-bounds..      4      0.19    0.1482      0.16    0.1357    0.1725    0.1435
Boundingbox-update..      4      0.89    0.7905      0.75    0.7792     0.815    0.7826
Compute Statistics..      4      1.96     1.833      1.62     1.802     1.775     1.816
ComputePotential....      4     10.52     10.24      9.88     10.21     10.23     10.22
Create Distr........      4      0.01  0.008063      0.01  0.007719      0.01  0.007891
External field eval.      4      7.07     7.296      6.49     6.714     6.795     6.971
SelfField total.....      4     22.53     22.61     21.75     21.97     22.15     22.29
SF: GreensFTotal....      4      6.75     6.554      6.18     6.364      6.55      6.48
TIntegration1.......      4      0.75    0.7411      0.56    0.7235      0.68    0.7348
TIntegration2.......      4      2.08     1.909      1.88      1.89     1.988     1.899
Write H5-File.......      4      0.03   0.04278      0.01   0.04262    0.0175   0.04273
Write Stat..........      4      0.34     2.741      0.16    0.1856     0.225    0.8251
